package com.example.consumer.service.direct;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author Huang_ShengSen
 * @Date 2023/7/10 11:39
 * @PackageName:com.example.consumer.service
 * @ClassName: NewDirectReceiver
 * @Description: TODO
 * @Version 1.0
 */
@Component
@RabbitListener(queues = "TestDirectQueue")//监听的队列名称 TestDirectQueue
public class NewDirectReceiver {

    @RabbitHandler
    public void process(Map testMessage) {
        System.out.println("第二个DirectReceiver消费者收到消息  : " + testMessage.toString());
    }

}
