package com.example.consumer.service.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author Huang_ShengSen
 * @Date 2023/7/10 11:43
 * @PackageName:com.example.consumer.service
 * @ClassName: a
 * @Description: TODO
 * @Version 1.0
 */
@Component
@RabbitListener(queues = "topic.woman")
public class TopicTotalReceiver {

    @RabbitHandler
    public void process(Map testMessage) {
        System.out.println("Total消费者收到消息 : " + testMessage.toString());
    }
}
