生产者
1.创建名称为TestDirectQueue的队列queue对象
2.创建名称为TestDirectExchange的交换机DirectExchange
3.将队列与交换机绑定，路由键设置为TestDirectRouting
4.发送消息，发送到TestDirectExchange交换机，路由键为TestDirectRouting，消息内容为map
rabbitTemplate.convertAndSend("TestDirectExchange", "TestDirectRouting", map);

消费者
1.创建类，创建方法，方法上添加注解
@RabbitHandler
@RabbitListener(queues = "TestDirectQueue")//监听的队列名称 TestDirectQueue
2.从map中取出消息主体
3.执行业务